local angle = 2*math.pi/5

return function( )
	-- record
	t = {}
	t[#t+1] = { new, { } }
	for x = 1, 3 do
		t[#t+1] = { moveto, { x*10, 0 } }
		for i = 1, 5 do
			t[#t+1] = { forward, { 1 } }
			t[#t+1] = { left, { angle } }
		end
	end
	t[#t+1] = { tell, { } }
	-- run
	for _,f in ipairs(t) do
		f[1](table.unpack(f[2]))
	end
end
