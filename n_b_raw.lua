local angle = 2*math.pi/5

return function( )
	-- record
	t = {}
	t[#t+1] = { func = new }
	for x = 1, 3 do
		t[#t+1] = { func = moveto, 10*x, 0 }
		for i = 1, 5 do
			t[#t+1] = { func = forward, 1 }
			t[#t+1] = { func = left, angle }
		end
	end
	t[#t+1] = { func = tell }
	-- run
	for _, callinfo in ipairs( t ) do
		callinfo.func( table.unpack( callinfo ) )
	end
end

