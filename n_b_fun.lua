local angle = 2*math.pi/5

-- pack one func + args into a table
function record( f, ... )
	return { func = f, ... }
end

-- take a list/array/sequence of `record`ed functions & run them
function run( list )
	for _, callinfo in ipairs( list ) do
		callinfo.func( table.unpack( callinfo ) )
	end
end

return function( )
	-- record
	t = {}
	t[#t+1] = record( new )
	for x = 1, 3 do
		t[#t+1] = record( moveto, 10*x, 0 )
		for i = 1, 5 do
			t[#t+1] = record( forward, 1 )
			t[#t+1] = record( left, angle )
		end
	end
	t[#t+1] = record( tell )
	-- run
	run( t )
end

