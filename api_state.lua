-- stateful API
local current
local sin, cos = math.sin, math.cos
function new( )  current = { angle = 0, x = 0, y = 0 }  end
-- directions are probably mixed up, doesn't matter for the timing purpose
function left( da )  current.angle = current.angle - da  end
function right( da )  current.angle = current.angle + da  end
function forward( dist )
	local dx, dy = dist*sin( current.angle ), dist*cos( current.angle )
	current.x, current.y = current.x + dx, current.y + dy
end
function moveto( x, y )
	current.x, current.y = x, y
end
function tell( )
	return current.x, current.y
end
