#!/bin/sh
lua main.lua | tee data.tsv
R --slave < graph.R
convert    plot_big.png -resize '50%'    plot.png && rm    plot_big.png
convert logplot_big.png -resize '50%' logplot.png && rm logplot_big.png
