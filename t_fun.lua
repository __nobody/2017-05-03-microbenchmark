local angle = 2*math.pi/5

local function record( f, ... )  return { f, { ... } }  end
local function run( t )
	for _,f in ipairs(t) do
		f[1](table.unpack(f[2]))
	end
end

return function( )
	-- record
	t = {}
	t[#t+1] = record( new )
	for x = 1, 3 do
		t[#t+1] = record( moveto, 10*x, 0 )
		for i = 1, 5 do
			t[#t+1] = record( forward, 1 )
			t[#t+1] = record( left, angle )
		end
	end
	t[#t+1] = record( tell )
	-- run
	run( t )
end
