# semi-crappy microbenchmark

## What?

See https://stackoverflow.com/q/43719322/805875 and the comments below my
answer.

## Code

### Dependencies

#### data generation

 *  Lua (any 5.x version(s))
     +  adjust `lua_binaries` in `main.lua` to point at all Lua
        interpreters that you want to test
 *  luasocket, for each Lua version to test
     +  only used for the `gettime()` call, you can substitute an
        equivalent function in `runner.lua` (`os.clock` is less exact but
        should work)

#### plotting

 *  R + ggplot2 for the included script
 *  graphicsmagick for image downscaling (poor man's anti-aliasing)

### Running

 *  adjust `main.lua` to your tastes
     +  `iterations` is the number of repetitions of the inner loop
     +  `repetitions` is how many runs with the same parameters are
         measured
     +  `lua_binaries` are the names (potentially with path) of all Lua
         binaries you want to test
     +  `fake_apis` are dummy APIs to simulate at least some "real" work
        (these live in `api_<name>.lua` and are `require`d in the runner
        so any defined functions are available to the scripts)
     +  `bench_scripts` are all the different code variations you want to
         measure (full script names), each of these script should return a
         single function when run (which will then be called in a loop)
 *  prepare for benchmarking (set fixed CPU frequency, kill CPU hogs, ...)
 *  run `main.sh`
     +  without bash and/or without R, just run `lua main.lua` and dump
        the output into a file (the script uses `data.tsv`)
 *  done (CPU can calm down again)
     +  without R, analyze/plot the data in `data.tsv` any way you like

### Output

#### `data.tsv`

tab-separated data (with one header line), columns are

 *  `version` (string): Lua version
 *  `script` (string): which of the `bench_scripts` is this one?
 *  `fakeapi` (string): which of the `fake_apis` did this use?
 *  `iters` (number): `iterations`
 *  `run` (number): counts up from `1` to `repetitions`
 *  `dt` (number): run time [seconds]
 *  `gctime` (number): time for final garbage collection [seconds]

#### `plot.png`, `logplot.png`

Linear and log<sub>2</sub>-plot of `data.tsv`.

## License

The files `t_fun.lua` and `t_raw.lua` include code by 'tonypdmtr' which
was posted on StackOverflow as an answer to the question linked above
(and [therefore](https://stackoverflow.com/help/licensing) is
[CC-BY-SA](https://creativecommons.org/licenses/by-sa/3.0/)).

All other files are written by me in entirety and are hereby placed into
the public domain.  (Or if that doesn't work where you are,
[CC-0](https://creativecommons.org/publicdomain/zero/1.0/).)

