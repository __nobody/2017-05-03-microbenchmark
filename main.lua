
iterations = 1e5  -- inner (timed) loop
repetitions = 10   -- measurement noise reduction

lua_binaries = { "lua5.1", "lua5.2", "lua5.3", "luajit", }

fake_apis = { "nop", "state" }

bench_scripts = {
	-- overhead measurements
	--"nop.lua", "call.lua",
	-- tonypdmtr
	"t_raw.lua", "t_fun.lua",
	-- nobody
	"n_a_fun.lua", "n_b_fun.lua", "n_b_raw.lua",
	-- added after observing first results
	"n_c_fun.lua", "n_c_raw.lua",
}

-- header
io.stdout:write "version\tscript\tfakeapi\titers\trun\tdt\tgctime\n"
io.stdout:flush( )
-- run benchmarks
for _, script in ipairs( bench_scripts ) do
	for _, api in ipairs( fake_apis ) do
		for _, bin in ipairs( lua_binaries ) do
			local prog = "%s runner.lua %d %d %s %s"
			os.execute( prog:format( bin, repetitions, iterations, script, api ) )
		end
	end
end

