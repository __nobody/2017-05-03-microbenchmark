local angle = 2*math.pi/5

return function( )
	-- record
	t = {}
	t[#t+1] = { new }
	for x = 1, 3 do
		t[#t+1] = { moveto, 10*x, 0 }
		for i = 1, 5 do
			t[#t+1] = { forward, 1 }
			t[#t+1] = { left, angle }
		end
	end
	t[#t+1] = { tell }
	-- run
	for _, callinfo in ipairs( t ) do
		callinfo[1]( table.unpack( callinfo, 2 ) )
	end
end

