-- usage: runner <repetitions> <iterations> <script> <api>
-- <script> should return a 'step function',
-- which is run <iterations> times in a loop.
-- Measurement is repeated <repetitions> times.
local repetitions = assert( tonumber( arg[1] ) )
local iterations = assert( tonumber( arg[2] ) )
local script = assert( arg[3] )
local api_name = assert( arg[4] )
assert( require( "api_"..api_name ) )
local step = assert( dofile( script ) )
script = script:sub( 1, -5 )

if not table.unpack then  table.unpack = unpack  end -- 5.1 compat

local gettime = require "socket".gettime

function report( run, dt, gc )
	local version = jit and "LuaJIT" or _VERSION
	local s = "%s\t%s\t%s\t%.4g\t%3d\t%.5f\t%.6f\n"
	io.stdout:write( s:format( version, script, api_name, iterations, run, dt, gc ) )
	io.stdout:flush( )
end

for i = 1, repetitions do
	-- initial full garbage collection for equal start
	collectgarbage "collect"
	collectgarbage "collect"
	-- determine main runtime
	local t0 = gettime()
	for _ = 1, iterations do  step( )  end
	local t1 = gettime()
	-- record collection time
	collectgarbage "collect"
	collectgarbage "collect"
	local t2 = gettime()
	-- report results
	report( i, t1-t0, t2-t1 )
end

